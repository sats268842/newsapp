import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../../Core/Api/api-service.service';
import { LandingService } from 'src/app/Abstraction/landing.service';
@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  events: string[] = [];
  opened: boolean;                                                                                                                                                                                                                                                                         
  mArticles:Array<any>;
  mSources:Array<any>;

  constructor(private newsapi:ApiServiceService, private land: LandingService){
    console.log('app component constructor called');  
    this.land.getRecentPosts().subscribe(data => {this.mArticles = data});
    this.land.getSearchSource().subscribe(data => {this.mArticles = data});   
  }

  ngOnInit() {
  }
}