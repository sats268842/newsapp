import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomeComponent} from './home.component';
import {PostListComponent} from './post-list/post-list.component';
import {SharedModule} from '../../Shared/shared.module';
import { ShellModule } from '../../shell/shell.module';
@NgModule({
  declarations: [HomeComponent,PostListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ShellModule
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
