import { Injectable } from '@angular/core';
import { ApiServiceService } from '../Core/Api/api-service.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LandingService {
  newsArticles: Array<any>;
  newsSources: Array<any>;
  recent = new Subject<any>();
  Sources = new Subject<any>();
  searchSource = new Subject<any>();

  constructor( private newsapi : ApiServiceService) { 

    this.newsapi.initArticles().subscribe(async data => {
      this.newsArticles = data['articles'];
      await this.newsArticles;
      this.setRecentAllPosts(this.newsArticles);
      this.newsapi.initSources().subscribe(async data => {
        this.newsSources = data['sources'];
        await this.newsSources;
        this.setAllSources(this.newsSources);
      });
    });
  }
  
  setRecentAllPosts(source: any) {
    this.recent.next(source);
  }

  getRecentPosts() {
    return this.recent.asObservable();
  }

  setAllSources(source: any) {
    this.Sources.next(source);
    this.Sources.next(source);
  }

  getAllSources() {
    return this.Sources.asObservable();
  }

  setSearchSource(source: any) {
    this.searchSource.next(source);
  }

  getSearchSource() {
    return this.searchSource.asObservable();
  }

  searchArticles(source) {
    this.newsapi.getArticlesByID(source).subscribe(data => { 
      this.newsArticles = data['articles']; 
      this.setSearchSource(this.newsArticles);
       console.log(this.newsArticles) });
  }

}


