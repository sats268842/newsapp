import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../Core/Api/api-service.service';
import { PostListComponent} from '../../Presentation/home/post-list/post-list.component';
import { LandingService } from 'src/app/Abstraction/landing.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  mArticles:Array<any>;
  mSources:Array<any>;
  searchsource: Array<any>;
  constructor(private newsapi : ApiServiceService , private land :LandingService) { 
 this.land.getAllSources().subscribe(data => {this.mSources = data;});
  }

  ngOnInit() {
  }
 Setsource(source){
   this.land.searchArticles(source);
 }
}
