import { NgModule } from '@angular/core';
import { MatButtonModule,
  MatMenuModule, 
  MatCardModule, 
  MatToolbarModule, 
  MatIconModule, 
  MatSidenavModule,
  MatListModule
 } from '@angular/material';


@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
  ],
  exports : [MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,]
})
export class MaterialModule { }
